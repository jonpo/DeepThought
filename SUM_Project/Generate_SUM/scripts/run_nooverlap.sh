#!/bin/bash

json_file=utterances.json
noise_dir=/path to the train part of WHAM! ambient noise dataset/ # path to train WHAM noises
out_json=../our_metadata_train/
n_mixtures=13900
n_speakers=2
stage=0

set -e
mkdir -p $out_json


if [[ $stage -le 0 ]]; then
    mkdir -p $out_json/sparse_${n_speakers}_0
    echo "Creating metadata for ${n_speakers} speakers and no overlap"
    python generate_metadata_no_overlap.py $json_file $noise_dir $out_json/sparse_${n_speakers}_0/metadata.json --n_mixtures $n_mixtures --n_speakers $n_speakers
fi
