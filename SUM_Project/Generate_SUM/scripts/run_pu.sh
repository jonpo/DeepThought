#!/bin/sh
librispeech_dir=/insert path to train-clean-100 part of LibriSpeech/
textgrid_dir=/insert path to train-clean-100 part of LibriSpeech alignment files/
out_file=utterances.json
ms=0.15

set -e


echo "Making the required json file for no-overlap mixtures"
python parse_utterances.py $librispeech_dir $textgrid_dir $out_file
