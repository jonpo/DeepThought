#!/bin/bash

no_overlap_metadata=../our_metadata_train/
out_json=../our_metadata_train/
all_overlap="0.2 0.4 0.6 0.8 1"
n_speakers=2
stage=0

set -e


if [[ $stage -le 0 ]]; then
    for ovr_ratio in $all_overlap; do
mkdir -p $out_json/sparse_${n_speakers}_${ovr_ratio}
    echo "Making metadata for ${n_speakers} speakers and overlap ${ovr_ratio}"
    python generate_metadata_overlap.py $no_overlap_metadata/sparse_${n_speakers}_0/metadata.json $out_json/sparse_${n_speakers}_${ovr_ratio}/metadata.json --ovr_ratio $ovr_ratio
    done
fi
