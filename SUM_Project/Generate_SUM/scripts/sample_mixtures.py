import os
from pathlib import Path
from tqdm import tqdm
import glob
import shutil
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("ouput_dir")
parser.add_argument("input_dir")
parser.add_argument('--n_speakers', type=int, default=2)

args = parser.parse_args()

# Parameters:
output_dir = args.output_dir
input_dir = args.input_dir
speaker = args.n_speakers

# Create SUM_n_mix:
sum_directory = 'SUM_{n_speaker}_mix'.format(n_speaker = speaker)
sum_path = os.path.join(output_dir, sum_directory)
#if os.path.exists(dir): # Scared to implement this.
#    shutil.rmtree(dir)
os.mkdir(sum_path)
print("Created directory '% s'" % sum_directory)

# Get list of dirs with the given speaker setting:
overlaps = []
for dir in sorted(os.listdir(input_dir)):
    if dir.startswith('sparse_{n_speaker}'.format(n_speaker = speaker)):
        overlaps.append(dir)


index_start = 0
index_end = 0
partition = 0

for overlap_dir in overlaps:
    samplerates = []
    for dir in sorted(os.listdir(input_dir + '/' + overlap_dir)):
        samplerates.append(dir)

    #print("Index Start:", index_start)
    #print("Index End:", index_end)

    for samplerate_dir in samplerates:

        # Create directory for samplerate:
        if not os.path.isdir(sum_path + '/' + samplerate_dir):
            sample_path = sum_path + '/' + samplerate_dir
            os.mkdir(sample_path)
            print("Created directory '% s'" % samplerate_dir, " in the output directory")
        
        source_dirs = []
        for dir in sorted(os.listdir(input_dir + '/' + overlap_dir + '/' + samplerate_dir)):
            source_dirs.append(dir)

        for source_dir in source_dirs:
            source_path = sample_path + '/' + source_dir
            if not os.path.isdir(source_path):
                os.mkdir(source_path)
                print("Created directory '% s'" % source_dir, " in the output directory")

            # Get list of files in the source:
            files = sorted(os.listdir(input_dir + '/' + overlap_dir + '/' + samplerate_dir + '/' + source_dir))
            # If this is not defined for the speaker (ie is 0) overwrite it:
            if partition == 0:
                partition = int(len(files) / len(overlaps))  
                index_end = partition                              

            # Now take the partition of files:
            files = files[index_start:index_end]


            # Copy the files one by one:
            print("Copying files from: ", input_dir + '/' + overlap_dir + '/' + samplerate_dir + '/' + source_dir)
            print("Copyting files to: ", source_path)
            for source_file in tqdm(files):
                
                original = input_dir + '/' + overlap_dir + '/' + samplerate_dir + '/' + source_dir + '/' + source_file
                target = source_path
                shutil.copy(original, target) 
    # Update slicers:
    index_start += partition
    index_end += partition

print("The program ran without incidents")