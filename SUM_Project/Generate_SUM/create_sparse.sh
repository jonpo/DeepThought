#!/bin/sh

librispeech_subdir=/path to train part of the LibriSpeech dataset/
noise_dir=/path to train part of WHAM! ambient noise dataset/
metadata_dir=./our_metadata_train
out_dir=../../data/our_output_train/ # output directory
stage=0
fs=8000
n_speakers=2
all_overlap="0.2 0.4 0.6 0.8 1"

set -e
mkdir -p $out_dir

if [[ $stage -le 0 ]]; then
    for ovr_ratio in 0 $all_overlap; do
      echo "Making mixtures for ${n_speakers} speakers and overlap ${ovr_ratio}"
      python scripts/make_mixtures.py $metadata_dir/sparse_${n_speakers}_${ovr_ratio}/metadata.json \
        $librispeech_subdir $out_dir/sparse_${n_speakers}_${ovr_ratio}/wav${fs} --noise_dir $noise_dir --rate $fs
      done
fi
