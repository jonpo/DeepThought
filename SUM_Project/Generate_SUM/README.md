# Sparse Utterance Mixture (SUM) dataset

### About the dataset
The SUM dataset contains two part: a train and a validation (dev) part.
Each part consists of in total seven partitions each containing mixtures with a specified overlap ratio, except the seventh partition containing an equal mixture of files from all other partitions.
This may be changed to suit the needs of the individual user.

### Generating a part of the SUM dataset
It is up to the user to generate the required number of parts (train or dev). A test set may also be generated, which is functionally similar to the SparseLibriMix testset.
To generate the desired part, the filepaths must be changed in all shell scripts to the described destinations, as well as within the ```librispeech_utils.py``` and the ```textgrid_utils.py``` files.
The, the ```create_dataset.sh``` file may be run, of if desired each shell file can be run individually in the following order:
* ./scripts/run_pu.sh
* ./scripts/run_nooverlap.sh
* ./scripts/run_overlap.sh
* ./create_sparse.sh
* ./scripts/run_sample.sh


---
### Note
Metadata is provided for the train part, for a two speaker setting with an 8 khz sampling rate. The montral forced alignment files can be found at: [here](https://zenodo.org/record/2619474#.XwiHMhF8Lg4).



