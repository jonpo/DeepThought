
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import scipy.stats as st
import os
import statsmodels.api as sm

abspath = os.path.abspath(__file__)
dname = os.path.dirname(abspath)
os.chdir(dname)
try:
    os.makedirs("../plots/ConvTasNet")
except FileExistsError:
    # directory already exists
    pass

#NOTE 

test_ratios = ['0', '0.2', '0.4', '0.6', '0.8', '1', 'mix']

si_sdr_diff_dict = dict([(key, []) for key in test_ratios])
pesq_diff_dict = dict([(key, []) for key in test_ratios])

test_results_si_sdri = []
test_results_pesq = []

for ratio in test_ratios:
    #Get test result from mix-trained model
    evalmix = pd.read_csv('exp/train_convtasnet_mix/eval_results_' + ratio + '/all_metrics.csv')
    evalmix["SI-SDRi_mix"] = evalmix["si_sdr"] - evalmix["input_si_sdr"]
    evalmix.drop(evalmix.columns.difference(['SI-SDRi_mix', 'mix_path', 'pesq']), 1, inplace=True)
    evalmix.rename(columns = {'pesq' : 'pesq_mix'}, inplace = True)

    #Get results from 100%-trained model
    eval100 = pd.read_csv('exp/train_convtasnet_1/eval_results_' + ratio + '/all_metrics.csv')
    eval100["SI-SDRi_100"] = eval100["si_sdr"] - eval100["input_si_sdr"]
    eval100.drop(eval100.columns.difference(['SI-SDRi_100', 'mix_path', 'pesq']), 1, inplace=True)
    eval100.rename(columns = {'pesq' : 'pesq_100'}, inplace = True)

    #Merge mix and 100% so that the values for the same speech mixtures area aligned. 
    df = evalmix.merge(eval100, how='inner', on = 'mix_path')
    #Make arrays for SI-SDRi
    si_sdri_mix = df.loc[:, 'SI-SDRi_mix'].values
    si_sdri_100 = df.loc[:, 'SI-SDRi_100'].values
    si_sdr_diff_dict[ratio] = (si_sdri_mix-si_sdri_100)

    #Compute t_test testing null-hypothesis that the mean difference SI-SDRi value between train-mix and train 100 is 0. 
    test_si_sdri = st.wilcoxon(si_sdri_mix, si_sdri_100)
    
    #One of the assumptions of the Wilcoxon signed-rank test is that the distribution of the differences should be symmetrical in shape. 
    #It is thus necessary to investigate this. 
    sns.histplot(si_sdri_mix - si_sdri_100)
    
    test_results_si_sdri.append(test_si_sdri)

    #Make arrays for PESQ
    pesq_mix = df.loc[:, 'pesq_mix'].values
    pesq_100 = df.loc[:, 'pesq_100'].values
        #Compute t_test testing null-hypothesis that the mean difference PESQ value between train-mix and train 100 is 0. 
    test_pesq = st.wilcoxon(pesq_mix, pesq_100)
    pesq_diff_dict[ratio] = (pesq_mix-pesq_100)
    
    #One of the assumptions of the Wilcoxon signed-rank test is that the distribution of the differences should be symmetrical in shape. 
    #It is thus necessary to investigate this. 
    sns.histplot(pesq_mix - pesq_100)

    test_results_pesq.append(test_pesq)

print("p-values of wilcoxon signed rank test between SI-SDRi values of MIX and 100% training sets for all overlap ratio test sets")
for i in range(len(test_ratios)):
    print("Test overlap: " + test_ratios[i] + ". p-value: " + str(test_results_si_sdri[i][1]) )


print("p-values of wilcoxon signed rank test between PESQ values of MIX and 100% training sets for all overlap ratio test sets")
for i in range(len(test_ratios)):
    print("Test overlap: " + test_ratios[i] + ". p-value: " + str(test_results_pesq[i][1]) )


# %%
############################################################
#                   Normal Distributions                   #
############################################################

plt.figure(figsize=(16,24))
num_rows = 7
num_cols = 2
num_plots = num_rows * num_cols
i=0             
k=0

color = ['#00A6A6', '#efca08']
for i, train in enumerate(test_ratios):
    for j, test in enumerate([si_sdr_diff_dict, pesq_diff_dict]): # Test loop
        plt.subplot(num_rows, num_cols,k+1)
        data = test[train]
        data_norm = (data - np.mean(data))/np.std(data)
        sns.distplot(data_norm, bins = 45, color = '#696969')
        plt.xticks(fontsize = 16)
        plt.yticks(fontsize = 16)
        if i == 0:
            plt.title(['SI SDRi', 'PESQ'][j], fontsize = 20)
        else:
            plt.title('')
        k+=1
        if(j == 0):
            plt.ylabel(train.capitalize(), fontsize = 20)
        else:
            plt.ylabel('')
        plt.xlabel('')
#plt.suptitle("Difference Between Mix and Full Overlap")
plt.savefig('../plots/ConvTasNet/diff_plot.eps', bbox_inches="tight")
plt.savefig('../plots/ConvTasNet/diff_plot.png', bbox_inches="tight")
plt.show()
plt.clf()
