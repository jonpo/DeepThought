import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import scipy.stats as st
import os
import statsmodels.api as sm

abspath = os.path.abspath(__file__)
dname = os.path.dirname(abspath)
os.chdir(dname)
try:
    os.makedirs("../plots/ConvTasNet")
except FileExistsError:
    # directory already exists
    pass

sns.set_theme(style="whitegrid")
sns.set(font_scale=1.8)
colors = ['#00A6A6', '#efca08', '#14213D', '#7284A8', '#474954', '#303633', '#606D5D', '#0B3954', '#E0FF4F']
sns.set_palette(sns.color_palette(colors))

exp_name = ['0', '20', '40', '60', '80', '100', 'Mix']
exp_tags = ['0', '0.2', '0.4', '0.6', '0.8', '1', 'mix']

measures = ['si_sdr', 'pesq']

def get_col(train_tag, test_tag, measure):
    path = 'exp/train_convtasnet_' + train_tag + '/eval_results_' + test_tag + '/all_metrics.csv'
    try:
        col1 = pd.read_csv(path, usecols = [measure]).squeeze()
        col2 = pd.read_csv(path, usecols = ['input_'+ measure]).squeeze()
    except:
        print("Train {0} with test result {1} is not available".format(train_tag, test_tag))
        return np.nan
    if measure == 'pesq':
        return col1
    else:
        return col1 - col2

for measure in measures:

    print(measure, '100%')
    df_dict = dict([(exp_tags[i], get_col('1', exp_tags[i], measure)) for i in range(len(exp_tags))])
    df1 = pd.DataFrame(df_dict)
    for col in list(df1.columns):
        data = df1[col]
        if col == 'mix':
            data = data.dropna()
        conf_interval = st.t.interval(alpha=0.95, df=len(data)-1, loc=np.mean(data), scale=st.sem(data))
        print(conf_interval[0].round(3), conf_interval[1].round(3))
    df1['Train'] = ['100% overlap']*len(df1)

    print(measure, 'mix')
    df_dict = dict([(exp_tags[i], get_col('mix', exp_tags[i], measure)) for i in range(len(exp_tags))])
    df2 = pd.DataFrame(df_dict)
    for col in list(df2.columns):
        data = df2[col]
        if col == 'mix':
            data = data.dropna()
        conf_interval = st.t.interval(alpha=0.95, df=len(data)-1, loc=np.mean(data), scale=st.sem(data))
        print(conf_interval[0].round(3), conf_interval[1].round(3))
    df2['Train'] = ['Mixed overlap']*len(df2)

    if measure == 'pesq':
        low = 1.5
        cur_ylabel = 'PESQ'
    else:
        low = 8
        cur_ylabel = 'SI-SDR improvement [dB]'

    df = pd.concat([df1,df2],ignore_index=True)
    df = pd.melt(df, id_vars="Train", var_name="Overlap", value_name=cur_ylabel)

    fig, ax = plt.subplots(figsize = (9,6), dpi = 400)

    ax = sns.barplot(x = 'Overlap', y = cur_ylabel, hue="Train", data=df, capsize=.15, errwidth=2, ci = 95)
    ax.set_ylim(bottom = low)
    ax.set_xlabel('Overlap ratio of test set (%)')
    ax.set_xticklabels(exp_name)
    plt.savefig('../plots/ConvTasNet/mix_and_100_' + measure + '.png', bbox_inches="tight")
    plt.savefig('../plots/ConvTasNet/mix_and_100_' + measure + '.eps', bbox_inches="tight")
    plt.clf()


    ###############################
    fig, ax = plt.subplots(figsize = (12,8), dpi = 400)
    matrix = np.empty((len(exp_tags),len(exp_tags)))
    # Plot 2
    for i, train in enumerate(exp_tags): # Train loop
        for j, test in enumerate(exp_tags): # Test loop
            matrix[i,j] = np.mean(get_col(train, test, measure))
                
    df = pd.DataFrame(data = matrix, index=exp_name, columns=exp_name)

    ax = sns.heatmap(df, annot=True,  fmt=".2f", cmap = 'viridis')
    plt.xlabel('Overlap ratio of test set (%)')
    plt.ylabel('Overlap ratio of training set (%)')
    pos, textvals = plt.yticks()
    plt.yticks(pos, exp_name, rotation=90, va="center")

    plt.savefig('../plots/ConvTasNet/matrix_' + measure + '.png', bbox_inches="tight")
    plt.savefig('../plots/ConvTasNet/matrix_' + measure + '.eps', bbox_inches="tight")

############################################################
#                   Normal Distributions                   #
############################################################
plt.figure(figsize=(36,27))
for measure in measures:
    num_rows = 7
    num_cols = 7
    num_plots = num_rows * num_cols
    i=0             
    k=0

    for i, train in enumerate(exp_tags):
        for j, test in enumerate(exp_tags): # Test loop
            plt.subplot(num_rows, num_cols,k+1)
            data = get_col(train, test, measure)
            data_norm = (data - np.mean(data))/np.std(data)
            sns.distplot(data_norm, bins = 30, hist_kws=dict(alpha=0.8))
            plt.xticks(fontsize = 16)
            plt.yticks(fontsize = 16)
            if i == 0:
                plt.title('Test: ' + exp_name[j].capitalize()+ "%", fontsize = 20)
            else:
                plt.title('')
            k+=1
            if(j == 0):
                plt.ylabel('Train: ' + exp_name[i].capitalize()+ "%", fontsize = 20)
            else:
                plt.ylabel('')
            plt.xlabel('')
plt.legend(['SI SDRi', 'PESQ'], loc="lower center", ncol = 2, bbox_to_anchor=(-3.15, -0.6))
plt.savefig('../plots/ConvTasNet/hist.eps', bbox_inches="tight")
plt.savefig('../plots/ConvTasNet/hist.png', bbox_inches="tight")
plt.show()
plt.clf()

for measure in measures:
    num_rows = 7
    num_cols = 7
    num_plots = num_rows * num_cols
    i=0             
    k=0

    for i, train in enumerate(exp_tags):
        for j, test in enumerate(exp_tags): # Test loop
            ax = plt.subplot(num_rows, num_cols,k+1)
            data = get_col(train, test, measure)
            data_norm = (data - np.mean(data))/np.std(data)
            sm.qqplot(data_norm, line = '45', ax=ax)
            plt.xticks(fontsize = 16)
            plt.yticks(fontsize = 16)
            plt.xlabel('')
            if i == 0:
                plt.title('Test: ' + exp_name[j].capitalize() + "%", fontsize = 20)
            else:
                plt.title('')
            k+=1
            if(j == 0):
                plt.ylabel('Train: ' + exp_name[i].capitalize()+ "%",fontsize = 20)
            else:
                plt.ylabel('')
    plt.savefig('../plots/ConvTasNet/qqplot_' + measure + '.eps', bbox_inches="tight")
    plt.savefig('../plots/ConvTasNet/qqplot_' + measure + '.png', bbox_inches="tight")
    plt.show()
    plt.clf()