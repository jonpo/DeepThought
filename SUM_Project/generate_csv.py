import argparse
import os
import pandas as pd
from os import listdir
from os.path import isfile, join
import soundfile as sf
from tqdm import tqdm

parser = argparse.ArgumentParser("Parsing folders creating csv files")
parser.add_argument("data_dir")

if __name__ == "__main__":
    args = parser.parse_args() 
    #data_dir = args.data_dir # Set the output folder.
    abspath = os.path.abspath(__file__)
    os.chdir(dname)
    main_dir = os.path.dirname(abspath).
    data_dir = os.path.join(main_dir, 'data')  
    
    #CHANGE THIS BETWEEN DEV, TRAIN and TEST
    setting = 'our_output_test'
    #############

    set_dir_data = os.path.join(data_dir, setting)
    set_dir = os.path.join(main_dir, setting)

    if not os.path.isdir(main_dir):
        os.mkdir(main_dir)
    if not os.path.isdir(data_dir):
        os.mkdir(data_dir)
    if not os.path.isdir(set_dir):
        os.mkdir(set_dir)
    if not os.path.isdir(set_dir_data):
        os.mkdir(set_dir_data)
    
    # Get the sparse folders:
    sparse_dir = []
    for root,dirs,files in os.walk(set_dir):
        if root[len(set_dir):].count(os.sep) == 0:
            rel_path = sorted(dirs)
            for d in rel_path:
                new_path = os.path.join(root,d)
                sparse_dir.append(os.path.join(new_path,'wav8000'))
    print("Done finding main folders! Next up, generating csv file...")

    def length_soundfile(the_file):
        soundfile = sf.SoundFile(the_file)
        return len(soundfile)

    for i, d in tqdm(enumerate(sparse_dir)):
        folder_dict = dict([(key, []) for key in sorted(os.listdir(d))])
        for fold in folder_dict.keys():
            folder_dict[fold] = [os.path.join(os.path.join(d, fold),elem) for elem in sorted(os.listdir(os.path.join(d, fold)))]
            #print([elem for elem in sorted(os.listdir(os.path.join(d, fold)))])
        df = pd.DataFrame.from_dict(folder_dict)
        df = df.drop(['mix_clean'], axis = 1)
        if len(df.columns) == 4: # If decide to use mix_clean, then change this to 5
            df = df.rename(columns={'mix_noisy' : 'mixture_path','s1' : 'source_1_path','s2' : 'source_2_path','noise' : 'noise_path'})
            df = df.reindex(columns = ['mixture_path','source_1_path','source_2_path','noise_path'])
        else:
            df = df.rename(columns={'mix_noisy' : 'mixture_path','s1' : 'source_1_path','s2' : 'source_2_path', 's3' : 'source_3_path', 'noise' : 'noise_path'})
            df = df.reindex(columns = ['mixture_path','source_1_path','source_2_path', 'source_3_path', 'noise_path'])
        df['length'] = df['mixture_path'].apply(length_soundfile)

        print(df)
        #print(os.path.join(main_dir + 'data/' + sub_dir + rel_path[i] + '_mixture.csv'))
        df.to_csv(os.path.join(set_dir_data, rel_path[i] + '_mixture_both.csv'), index = False)


    # mixture_ID,mixture_path,source_1_path,source_2_path,noise_path,length