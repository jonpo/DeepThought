#!/bin/bash
tag_list='0 0.2 0.4 0.6 0.8 1 mix'

for my_tag in $tag_list
do
	sh ConvTasNet/run.sh --stage 2 --tag ${my_tag} --task sep_noisy --id 0 --number_of_examples 10
done
echo "All done"