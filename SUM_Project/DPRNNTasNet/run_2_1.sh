#!/bin/bash
#!/bin/sh
#BSUB -J DPRRN_sparse_2_1
#BSUB -o DPRRN_sparse_2_1_%J.out
#BSUB -e DPRRN_sparse_2_1_%J.err
#BSUB -q gpuv100
#BSUB -n 2
#BSUB -gpu "num=2:mode=exclusive_process"
#BSUB -R "rusage[mem=5G]"
#BSUB -R "select[gpu32gb]"
#BSUB -u jonpo@dtu.dk
#BSUB -N
#BSUB -W 24:00
# end of BSUB options
source ../asteroid-env/bin/activate

nvidia-smi # GPU status

# Exit on error
set -e
set -o pipefail

# Path to the python you'll use for the experiment. Defaults to the current python
# You can run ./utils/prepare_python_env.sh to create a suitable python environment, paste the output here.
python_path=../asteroid-env/bin/python3

# Example usage
# ./run.sh --stage 3 --tag my_tag --task sep_noisy --id 0,1

# General
number_of_examples=10
stage=3  # Controls from which stage to start
tag="1"  # Controls the directory name associated to the experiment
test_set_names='0 0.2 0.4 0.6 0.8 1 mix'
from_checkpoint=epoch=51-step=89699.ckpt
# You can ask for several GPUs using id (passed to CUDA_VISIBLE_DEVICES)
id=0,1 #$CUDA_VISIBLE_DEVICES

# Training config
epochs=100
batch_size=4 # was 2
num_workers=4
half_lr=yes
early_stop=yes
# Optim config
optimizer=adam
lr=0.001
weight_decay=0.
# Data config
sample_rate=8000
mode=min
n_src=2
segment=3 # Was 1
task=sep_noisy  # one of 'enh_single', 'enh_both', 'sep_clean', 'sep_noisy'


eval_use_gpu=1
# Need to --compute_wer 1 --eval_mode max to be sure the user knows all the metrics
# are for the all mode.
compute_wer=0
#eval_mode=

. utils/parse_options.sh


#sr_string=$(($sample_rate/1000))
#suffix=wav${sr_string}k/$mode

#if [ -z "$eval_mode" ]; then
#  eval_mode=$mode
#fi

train_dir=../data/our_output_train/sparse_2_${tag}
valid_dir=../data/our_output_dev/sparse_2_${tag}

if [[ $stage -le  0 ]]; then
	echo "Stage 0: Generating Librimix dataset"
  . local/generate_librimix.sh --storage_dir $storage_dir --n_src $n_src
fi

if [[ $stage -le  1 ]]; then
	echo "Stage 1: Generating csv files including wav path and duration"
  . local/prepare_data.sh --storage_dir $storage_dir --n_src $n_src
fi

# Generate a random ID for the run if no tag is specified
uuid=$($python_path -c 'import uuid, sys; print(str(uuid.uuid4())[:8])')
if [[ -z ${tag} ]]; then
	tag=${uuid}
fi

expdir=exp/train_dprnntasnet_${tag}
mkdir -p $expdir && echo $uuid >> $expdir/run_uuid.txt
echo "Results from the following experiment will be stored in $expdir"


if [[ $stage -le 2 ]]; then
  echo "Stage 2: Training"
  mkdir -p logs
  CUDA_VISIBLE_DEVICES=$id $python_path train.py --exp_dir $expdir \
		--epochs $epochs \
		--batch_size $batch_size \
		--num_workers $num_workers \
		--half_lr $half_lr \
		--early_stop $early_stop \
		--optimizer $optimizer \
		--lr $lr \
		--weight_decay $weight_decay \
		--train_dir $train_dir \
		--valid_dir $valid_dir \
		--sample_rate $sample_rate \
		--n_src $n_src \
		--task $task \
		--from_checkpoint $from_checkpoint \
		--segment $segment | tee logs/train_${tag}.log
	cp logs/train_${tag}.log $expdir/train.log
#		
	# Get ready to publish
	mkdir -p $expdir/publish_dir
	echo "librimix/DPRNNTasNet" > $expdir/publish_dir/recipe_name.txt
fi

for test_set_name in $test_set_names
do
	echo $test_set_name
	out_dir=eval_results_${test_set_name} # Controls the directory name associated to the evaluation results inside the experiment directory
	test_dir=../data/our_output_test/sparse_2_${test_set_name}
	if [[ $stage -le 3 ]]; then
		echo "Stage 3 : Evaluation"

		if [[ $compute_wer -eq 1 ]]; then
			if [[ $eval_mode != "max" ]]; then
				echo "Cannot compute WER without max mode. Start again with --stage 2 --compute_wer 1 --eval_mode max"
				exit 1
			fi

			# Install espnet if not instaled
			if ! python -c "import espnet" &> /dev/null; then
				echo 'This recipe requires espnet. Installing requirements.'
				$python_path -m pip install espnet_model_zoo
				$python_path -m pip install jiwer
				$python_path -m pip install tabulate
			fi
		fi

	$python_path eval.py \
		--exp_dir $expdir \
		--test_dir $test_dir \
		--out_dir $out_dir \
		--use_gpu $eval_use_gpu \
		--compute_wer $compute_wer \
		--n_save_ex $number_of_examples \
		--task $task | tee logs/eval_${tag}.log

		cp logs/eval_${tag}.log $expdir/eval_${test_set_name}.log
	fi
done
echo "All done"
