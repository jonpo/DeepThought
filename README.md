# DeepThought

## 0. Findings
All the experiments that has been discribed in the report can be found ```DeepThought/SUM_Project/exp```. Each folder contains the model corresponding to the name of the folder, and inside each experiment folder, results from evaluations of each test set. 

## 1. Installing the SUM environment
During the creation of the project, we have used and only tested the whole pipeline using an venv based on Python 3.8.4. We recommend creating an virtual environment and use the requirements.txt for downloading the right libaries. More specifically, load the environment and used the following command from the SUM_Project folder:

```pip install -r requirements.txt```

**Known issues**
If installing on windows machine using python 3.8, run ```pip install pywin32==225```

## 2. LibriMix Download
The whole project is based on the original LibriMix dataset. Therefore, at the moment it is nessecary to clone the LibriMix repo and generate LibriMix before constructing the Sparse Utterance Mixtures (SUM) sparsely overlapped dataset [[1]](#1). To be clear, the location of the LibriMix folders (Libri2Mix, Libri3Mix, LibriSpeech, wham_noise) is the main directory.

```
git clone https://github.com/JorisCos/LibriMix
cd LibriMix 
./generate_librimix.sh storage_dir
```
Make sure that SoX is installed.

For windows :
```
conda install -c groakat sox
```

For Linux :
```
conda install -c conda-forge sox
```

### 2.1 Recreating results
To recreate reported results, it is only necessary to download the 2-speaker setting and 8k frequency. By changing last lines in ```generate_librimix.sh``` after having cloned the LibriMix reprository. 

## 3. Unzip 'librispeech_allignments'
Allignmentfiles are needed for creating the sparse trainingset. The zip file should be unzipped in the ```DeepThought/SUM_Project``` directory. The whole 623 MB zip file can be downloaded here: https://zenodo.org/record/2619474/files/librispeech_alignments.zip?download=1 [[2]](#2)


## 4. Generating SUM
Step 1-3 has to be completed before continuing. The fourth step is to run the following script: ```DeepThought/SUM_Project/Generate_SUM/create_sparse.sh```

## 5. Train and evaluate models
To train and evaluate all Conv-TasNet, you can simply run the  bash script located in ```DeepThought/SUM_Project/run_train_and_test.sh```. The DPRNN-TasNet can be trained and evaluated using the two run scripts located in ```DeepThought/SUM_Project/DPRNNTasNet ```.

The next section will give a more in depth description of which setting could be relevant to change and why. Note that most settings for the Conv-TasNet should be changed in ```DeepThought/SUM_Project/ConvTasNet/run.sh```

### 5.1 Changing the run.sh scripts
```python_path``` insert the python path. The environment should follow the rquirements.txt in the ```DeepThought/SUM_Project``` directory
```stage``` should be either 2 for training a new model or 3 for evaluating the model.
```number_of_examples``` the number of examples (sound examples) you want per model evaluation
```test_set_names``` Which datasets you want the model to be evaluated on.
```id``` change cuda device(s). Seperate with comma.

## 6 Investigating Results
Two scripts for each type of model has been created for collecting plots, tests, means, confidence intervals etc. These are the ```data_plot.py```  and ```t_test.py``` both lying in each of thge two model folders under ```DeepThought/SUM_Project```.

### 6.1 Using the models
Furthermore, by using Asteroids simple model implementation pipeline, a iPython notebook [```Results_notebook.ipynb```](SUM_Project/Results_notebook.ipynb) has been constructed to investigate single mixtures. The notebook uses the pretrained models trained for the purpose of this project. One can simply add a new folder for their .wav file and use the dropdowns to select sound to separate and the wanted model. The model is the downloaded using [Hugging Face](https://huggingface.co/jonpodtu)

## 7 Our findings
Mean SI-SDRi results for the 7 Conv-TasNet models     |  Mean PESQ results for the 7 Conv-TasNet models  
:----------------------------------------------------:|:----------------------------------------------------:
![](SUM_Project/plots/ConvTasNet/matrix_si_sdr.png)   |  ![](SUM_Project/plots/ConvTasNet/matrix_pesq.png)


## References
<a id="1">[1]</a> 
Joris Cosentino, Manuel Pariente, Samuele Cornell, Antoine Deleforge, & Emmanuel Vincent. (2020). LibriMix: An Open-Source Dataset for Generalizable Speech Separation.

<a id="2">[2]</a> 
Loren Lugosch. (2019, March 31). LibriSpeech Alignments (Version 1.0). Zenodo. http://doi.org/10.5281/zenodo.2619474
